package service

import (
	"context"

	"gitlab.com/market2075343/branch_service/config"
	"gitlab.com/market2075343/branch_service/genproto/branch_service"
	"gitlab.com/market2075343/branch_service/grpc/client"
	"gitlab.com/market2075343/branch_service/pkg/logger"
	"gitlab.com/market2075343/branch_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SmenaService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*branch_service.UnimplementedSmenaServiceServer
}

func NewSmenaService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SmenaService {
	return &SmenaService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SmenaService) Create(ctx context.Context, req *branch_service.SmenaCreateReq) (*branch_service.SmenaCreateResp, error) {
	u.log.Info("====== Smena Create ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) GetList(ctx context.Context, req *branch_service.SmenaGetListReq) (*branch_service.SmenaGetListResp, error) {
	u.log.Info("====== Smena Get List ======", logger.Any("req", req))

	resp, err := u.strg.Smena().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getting smena list", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) GetById(ctx context.Context, req *branch_service.SmenaIdReq) (*branch_service.Smena, error) {
	u.log.Info("====== Smena Get by Id ======", logger.Any("req", req))

	resp, err := u.strg.Smena().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while getting smena by id", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) Update(ctx context.Context, req *branch_service.SmenaUpdateReq) (*branch_service.SmenaUpdateResp, error) {
	u.log.Info("====== Smena Update ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Update(ctx, req)
	if err != nil {
		u.log.Error("error while updating smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) Delete(ctx context.Context, req *branch_service.SmenaIdReq) (*branch_service.SmenaDeleteResp, error) {
	u.log.Info("====== Smena Delete ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while deleting smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) CheckSmena(ctx context.Context, req *branch_service.SmenaCheckReq) (*branch_service.SmenaCheckResp, error) {
	u.log.Info("====== Smena Check ======", logger.Any("req", req))

	resp, err := u.strg.Smena().CheckSmena(ctx, req)
	if err != nil {
		u.log.Error("error while deleting smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
