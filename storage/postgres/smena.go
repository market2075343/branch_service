package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/market2075343/branch_service/genproto/branch_service"
)

type SmenaRepo struct {
	db *pgxpool.Pool
}

func NewSmenaRepo(db *pgxpool.Pool) *SmenaRepo {
	return &SmenaRepo{
		db: db,
	}
}

// 1. Smena crud

func (r *SmenaRepo) Create(ctx context.Context, req *branch_service.SmenaCreateReq) (*branch_service.SmenaCreateResp, error) {
	var id int
	query := `
	INSERT INTO smena (
		staff_id,
		branch_id,
		sale_amount,
		status
	)
	VALUES ($1,$2,$3,$4) RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.StaffId,
		req.BranchId,
		req.SaleAmount,
		req.Status,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &branch_service.SmenaCreateResp{
		Msg: fmt.Sprintf("%d", id),
	}, nil
}

func (r *SmenaRepo) GetList(ctx context.Context, req *branch_service.SmenaGetListReq) (*branch_service.SmenaGetListResp, error) {
	var (
		filter  = " WHERE finished_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		staff_id,
		branch_id,
		sale_amount,
		status,
		created_at::TEXT,
		updated_at::TEXT 
	FROM smena `

	if req.StaffId != "" {
		filter += ` AND staff_id = '` + req.StaffId + "' "
	}
	if req.BranchId != "" {
		filter += ` AND branch_id = '` + req.BranchId + "' "
	}
	if req.Status != "" {
		filter += ` AND status = '` + req.Status + "' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM smena` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	if err = r.db.QueryRow(ctx, countS).Scan(&count); err != nil {
		return nil, err
	}

	resp := &branch_service.SmenaGetListResp{}
	for rows.Next() {
		var smena = branch_service.Smena{}
		err := rows.Scan(
			&smena.Id,
			&smena.StaffId,
			&smena.BranchId,
			&smena.SaleAmount,
			&smena.Status,
			&smena.CreatedAt,
			&smena.UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Smenas = append(resp.Smenas, &smena)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *SmenaRepo) GetById(ctx context.Context, req *branch_service.SmenaIdReq) (*branch_service.Smena, error) {
	query := `
    SELECT 
		id,
		staff_id,
		branch_id,
		sale_amount,
		status,
		created_at::TEXT,
		updated_at::TEXT 
    FROM smena 
    WHERE id = $1;`

	var smena = branch_service.Smena{}

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&smena.Id,
		&smena.StaffId,
		&smena.BranchId,
		&smena.SaleAmount,
		&smena.Status,
		&smena.CreatedAt,
		&smena.UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &smena, nil
}

func (r *SmenaRepo) Update(ctx context.Context, req *branch_service.SmenaUpdateReq) (*branch_service.SmenaUpdateResp, error) {
	query := `
    UPDATE smena 
    SET 
        staff_id = $2,
        sale_amount = $4,
		status = $5,
		updated_at = NOW()
    WHERE id = $1 AND branch_id = $3;`

	_, err := r.db.Exec(ctx, query,
		req.Id,
		req.StaffId,
		req.BranchId,
		req.SaleAmount,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &branch_service.SmenaUpdateResp{
		Msg: "smena updated",
	}, nil
}

func (r *SmenaRepo) Delete(ctx context.Context, req *branch_service.SmenaIdReq) (*branch_service.SmenaDeleteResp, error) {
	query := `
    UPDATE smena 
    SET 
        finished_at = NOW()
    WHERE id = $1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}

	if res.RowsAffected() == 0 {
		return nil, fmt.Errorf("smena not found")
	}

	return &branch_service.SmenaDeleteResp{
		Msg: "smena deleted",
	}, nil
}

func (r *SmenaRepo) CheckSmena(ctx context.Context, req *branch_service.SmenaCheckReq) (*branch_service.SmenaCheckResp, error) {
	var count int
	query := `
	SELECT COUNT(*) FROM smena
	WHERE staff_id = $1 AND branch_id = $2 AND status='opened';`

	if err := r.db.QueryRow(ctx, query, req.StaffId, req.BranchId).Scan(&count); err != nil {
		return nil, err
	}

	if count > 0 {
		return &branch_service.SmenaCheckResp{Msg: "yes"}, nil
	} else {
		return &branch_service.SmenaCheckResp{Msg: "no"}, nil
	}
}
