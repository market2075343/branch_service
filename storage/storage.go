package storage

import (
	"context"

	"gitlab.com/market2075343/branch_service/genproto/branch_service"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	BranchProduct() BranchProductRepoI
	Smena() SmenaRepoI
}

type BranchRepoI interface {
	Create(ctx context.Context, req *branch_service.BranchCreateReq) (*branch_service.BranchCreateResp, error)
	GetList(ctx context.Context, req *branch_service.BranchGetListReq) (*branch_service.BranchGetListResp, error)
	GetById(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.Branch, error)
	Update(ctx context.Context, req *branch_service.BranchUpdateReq) (*branch_service.BranchUpdateResp, error)
	Delete(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.BranchDeleteResp, error)
}

type BranchProductRepoI interface {
	Create(ctx context.Context, req *branch_service.BranchProductCreateReq) (*branch_service.BranchProductCreateResp, error)
	GetList(ctx context.Context, req *branch_service.BranchProductGetListReq) (*branch_service.BranchProductGetListResp, error)
	GetById(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProduct, error)
	Update(ctx context.Context, req *branch_service.BranchProductUpdateReq) (*branch_service.BranchProductUpdateResp, error)
	Delete(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProductDeleteResp, error)
}

type SmenaRepoI interface {
	Create(ctx context.Context, req *branch_service.SmenaCreateReq) (*branch_service.SmenaCreateResp, error)
	GetList(ctx context.Context, req *branch_service.SmenaGetListReq) (*branch_service.SmenaGetListResp, error)
	GetById(ctx context.Context, req *branch_service.SmenaIdReq) (*branch_service.Smena, error)
	Update(ctx context.Context, req *branch_service.SmenaUpdateReq) (*branch_service.SmenaUpdateResp, error)
	Delete(ctx context.Context, req *branch_service.SmenaIdReq) (*branch_service.SmenaDeleteResp, error)
	CheckSmena(ctx context.Context, req *branch_service.SmenaCheckReq) (*branch_service.SmenaCheckResp, error)
}
